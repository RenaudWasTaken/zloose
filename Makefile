CC      = clang
CFLAGS  = -std=c99 -pedantic -Wall -Wextra -Werror -O3
LDLIBS  =
SRC     = main.c commands.c utils.c
OBJ     = $(addprefix $(BUILD_DIR)/, $(SRC:.c=.o))
EXEC    = zloose
SRC_DIR = src

ifndef
  BUILD_DIR=_build
endif

ifeq ($(strip $(BUILD_DIR)),)
  BUILD_DIR:=_build
endif

all: $(EXEC)

debug: CFLAGS += -g3
debug: CFLAGS := $(filter-out -O3, $(CFLAGS))
debug: all

$(EXEC): $(OBJ)
	$(LINK.c) -o $(EXEC) $^ $(LDLIBS)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	mkdir -p $(@D)
	$(COMPILE.c) -o $@ $^

clean:
	$(RM) $(OBJ)
	rm -rf $(BUILD_DIR)
	$(RM) $(EXEC)
