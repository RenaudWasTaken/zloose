#include "zloose.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "commands.h"

int rot = 0;

int get_index(char *cmd)
{
  char *white_cmds[] = { "ls", "man", "cat", "echo", "less",
    "most", "head", "tail", "time", "grep", "cut", "paste", "diff",
    "sort", "wc", "uniq", "man", "date", "bc", "dirname", "basename",
    "test", "printf", "yes", "true", "nc"};
  if (!strcmp(cmd, "passwd"))
    return 1;
  if (!strcmp(cmd, "bash") || !strcmp(cmd, "sh") || !strcmp(cmd, "zsh"))
    return 2;

  for (unsigned long i = 0; i < sizeof (white_cmds) / sizeof (char *); ++i)
    if (!strcmp(cmd, white_cmds[i]))
      return 3;
  if (!strcmp(cmd, "cd"))
    return 4;

  return 0;
}
char *goto_end(char *str) { while (*str++) { continue; } return --str; }

int is_rot_command(char *command) {
  char *rotated_commands[] = {"cat", "head", "tail", "grep", "diff"};
  for (unsigned long i = 0; i < sizeof (rotated_commands) / sizeof (char *); ++i)
    if (!strcmp(command, rotated_commands[i]))
      return 1;

  return 0;
}

void print_ps1(int ps1, char *pwd)
{
  char *path = strdup(pwd);
  char *end = goto_end(path);
  *(--end) = '\0';

  if (ps1 == 0)
    printf("[gauber_r: %s]$ ", path);
  else if (ps1 == 1)
    printf("$ ");
  else if (ps1 == 2)
    printf("dump2017%% ");

  free(path);
}

void init_cmds(void (**cmds)(char *cmd, char *params)) {
  cmds[0] = &nothing;
  cmds[1] = &passwd;
  cmds[2] = &handle_new_shell;
  cmds[3] = &handle_allowed_cmd;
  cmds[4] = &handle_cd;
}

void handle_command(char *cmd, char *svptr, void (**cmds)(char *cmd, char *params)) {
  cmds[get_index(cmd)](cmd, svptr);
}

int is_str_delim(char c, char prev, char prev_prev) {
  if (!prev && !prev_prev && (c == '\'' || c == '"'))
    return 1;

  if (!prev_prev && prev != '\\' && (c == '\'' || c == '"'))
    return 1;

  if (prev_prev != '\\' && prev != '\\' && (c == '\'' || c == '"'))
    return 1;

  if (prev_prev == '\\' && prev != '\\' && (c == '\'' || c == '"'))
    return 1;

  return 0;
}

void sanitize(char *line)
{
  int is_in_string = 0;
  for (int i = 0; line[i]; ++i)
  {
    if ((i == 0 && is_str_delim(line[i], '\0', '\0'))
       || (i == 1 && is_str_delim(line[i], line[i - 1], '\0'))
       || (is_str_delim(line[i], line[i - 1], line[i - 2])))
      is_in_string = !is_in_string;

    if ((!is_in_string &&
       (line[i] == ';' || (line[i] == '&' && line[i + 1] == '&')
        || line[i] == '|' || line[i] == '<' || line[i] == '>')) || line[i] == '`'
        || line[i] == '$') {
      printf("You loose ('|', '||', '&&', ';', '>', '<', '$', '`') not accepted\n");
      line[i] = '\0';
      return;
    }
  }
}
