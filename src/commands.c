#include "zloose.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>

extern char *current_cmd;
extern char *pwd;
extern int rot;

int rot13b(int c, int basis){
  c = (((c-basis)+13)%26)+basis;
  return c;
}

int rot13(int c){
  if('a' <= c && c <= 'z'){
    return rot13b(c,'a');
  } else if ('A' <= c && c <= 'Z') {
    return rot13b(c, 'A');
  } else {
    return c;
  }
}

void passwd()
{
  char *password = getpass("Password: ");
  if (!strcmp(password, PASSWD))
    execl("/bin/sh", "sh", "-c", "perl ~/libs/mini/zloose/scripts/zlock.plx unlock", NULL);
}

void handle_allowed_cmd(char *cmd, char *params) {
  (void) cmd; (void) params;
  char *exec_cmd = malloc(strlen(pwd) + strlen(current_cmd) + 8);
  sprintf(exec_cmd, "cd %s && %s", pwd, current_cmd);
  FILE *pipe = popen(exec_cmd, "r");
  char line[81];

  while(fgets(line, 80, pipe) != NULL) {
    line[80] = '\0';
    if (rot)
      for (int i = 0; i < 80; ++i) line[i] = rot13(line[i]);
    printf ("%s", line);
   }
  pclose(pipe);
}

void handle_cd(char *cmd, char *params)
{
  (void)cmd;
  if (params[0] == '/' || params[0] == '~') {
    free(pwd); pwd = NULL;
    int size = strlen(params);
    pwd = malloc((size + 2) * sizeof (char));
    strcpy(pwd, params);
    if (pwd[size - 1] != '/') { pwd[size] = '/'; pwd[size + 1] = '\0'; }
  } else {
    int size = strlen(pwd) + strlen(params) + 1;
    char *path = malloc(size * sizeof (char));
    strcpy(path, pwd); strcpy(path + strlen(path), params);
    free(pwd); pwd = path;
    size--;
    if (pwd[size - 1] != '/') { pwd[size] = '/'; pwd[size + 1] = '\0'; }
  }
}

void nothing(char *cmd, char *params) { (void)params; (void)cmd; printf("You loose !\n"); }
