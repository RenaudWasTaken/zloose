#include "zloose.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/signalfd.h>
#include <signal.h>

#include "commands.h"
#include "utils.h"

int ps1 = 0;
char *current_cmd = NULL;
char *pwd = NULL;
extern int rot;

void handle_new_shell(char *cmd, char *params) {
  (void)params;
  if (!strcmp(cmd, "bash"))
    ps1 = 0;
  else if (!strcmp(cmd, "sh"))
    ps1 = 1;
  else if (!strcmp(cmd, "zsh"))
    ps1 = 2;
}

void cleanup(char **input) {
  free(*input); *input = NULL;
  free(current_cmd); current_cmd = NULL;
}

int main(void)
{
  char *input = NULL;
  size_t n = 0;
  void (*cmd[COMMANDS]) (char *cmd, char *params);
  init_cmds(cmd);
  pwd = strdup("~/");

  char *command = NULL, *svptr = NULL;

  while (1) {
    rot = 0;
    cleanup(&input);
    print_ps1(ps1, pwd);

    getline(&input, &n, stdin);
    sanitize(input);
    char *end = goto_end(input);
    *(--end) = '\0';

    current_cmd = strdup(input);
    command = strtok_r(input, " \n", &svptr);
    if (!command) { continue; }
    rot = is_rot_command(command);
    handle_command(command, svptr, cmd);
  }
}
