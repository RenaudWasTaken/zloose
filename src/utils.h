#ifndef UTILS_H
# define UTILS_H

void print_ps1(int ps1, char *pwd);
int get_index(char *cmd);
void sanitize(char *line);
void handle_command(char *cmd, char *svptr, void (**cmds)(char *cmd, char *params));
void init_cmds(void (**cmds)(char *cmd, char *params));
char *goto_end(char *str);
int is_rot_command(char *command);

#endif
