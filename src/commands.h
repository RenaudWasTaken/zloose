#ifndef COMMANDS_H
# define COMMANDS_H

void passwd();
void handle_allowed_cmd(char *cmd, char *params);
void nothing(char *cmd, char *params);
void handle_new_shell(char *cmd, char *params);
void handle_cd(char *cmd, char *params);

#endif
