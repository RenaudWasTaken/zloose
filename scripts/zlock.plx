#! /usr/bin/perl

if ($#ARGV + 1 != 1 && $#ARGV + 1 != 2) { usage(); }

sub usage() { print "Usage: $0 [lock | unlock] {script}\n"; exit 1; }

if ($ARGV[0] eq 'lock') {
  if ($#ARGV + 1 != 2) { usage(); }
  if (!-e $ARGV[1]) { print("File does not exist\n"); usage(); }
  `cp ~/.i3/config ~/.i3/config_bak`;
  `cp $ARGV[1] ~/.i3/config`;
  `i3-msg restart`;
  `killall firefox`;
  `killall xterm`;
} elsif ($ARGV[0] eq 'unlock') {
  `cp ~/.i3/config_bak ~/.i3/config`;
  `i3-msg restart`;
} else { usage(); }
